﻿/*Запрос на получение подробной информации о сделках с производителем*/
Select status, first_name, second_name, third_name, price, amount, model, speed, color,
name as name_manufacturer from
(select * from
(select * from
(select * from
(select * from sellers inner join order_list_from_manufacturer 
on sellers.id_sellers = order_list_from_manufacturer.id_sellers)
as a inner join car_catalog_manufacturer 
on a.id_car_catalog_manufacturer = car_catalog_manufacturer.id_car_catalog_manufacturer)
as b inner join car
on b.id_car = car.id_car)
as c inner join manufacturer
on c.id_manufacturer = manufacturer.id_manufacturer)
as d
