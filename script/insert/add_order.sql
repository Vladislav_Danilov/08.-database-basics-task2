﻿/*добавление заказа по характеристикам автомобиля и имени продавца*/
insert into order_list (id_customer, id_sellers, id_car_catalog, status)
values(

(select id_customer from customer
where first_name='Семенова' 
and
second_name = 'Елена'
and
third_name ='Викторовна'), 

(select id_sellers from sellers
where first_name='Семенов' and second_name='Семен' 
and third_name='Семенович'), 

(select id_car_catalog from car_catalog
where price > 100000.00
and amount > 0
and id_car = (Select id_car from car
where model='Focus' and speed = 120 and color = 'Белый металлик' )), 

'Продано');

